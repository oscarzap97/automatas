package trayectorias;

import java.awt.event.*;
import modelos.*;

public class OyenteTrayectorias extends MouseAdapter {

  private final PanelTrayectorias panel;
  private final Puntos planetas;
  private boolean bandera;

  public OyenteTrayectorias(PanelTrayectorias panel, Puntos lista) {
    this.planetas = lista;
    this.panel = panel;
  }

  @Override
  public void mouseClicked(MouseEvent e){
      if(!bandera){
          bandera=true;
          for(Punto planeta:planetas){
              new HiloTrayectoria(planeta).start();
              
          }
      }
    }  
      class HiloTrayectoria extends Thread{
          private final Planeta planeta;
          
          public HiloTrayectoria(Punto planeta){
              this.planeta = (Planeta)planeta;
          }
          
          public void run(){
              int angulo = 0;
              int dX = planeta.getAnchoOrbita() /2;
              int dY = planeta.getAltoOrbita() /2;
              int tiempo = planeta.getTiempo();
              
              for(;;){
                  double rad = Math.toRadians(angulo);
                  planeta.x = (int) (dX * Math.cos(rad));
                  planeta.y = (int) (dY * Math.sin(rad));
                  angulo+=5;
                  try{
                      Thread.sleep(tiempo);
                  }catch (InterruptedException ex) {
                    System.out.println("Error de Hilo");
                    System.exit(-1); 
            }
            panel.repaint();
        }
    }
}
