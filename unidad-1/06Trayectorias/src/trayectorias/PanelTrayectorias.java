package trayectorias;

import java.awt.*;
import javax.swing.JPanel;
import modelos.Puntos;

public class PanelTrayectorias extends JPanel {

  private final Puntos planetas;

  public PanelTrayectorias(Puntos lista) {
    this.planetas = lista;
    setBackground(Color.BLACK);
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.translate(this.getWidth() / 2, this.getHeight() / 2);
    planetas.dibujar(g);
  }

  public void addEventos(OyenteTrayectorias o) {
    this.addMouseListener(o);
  }
}
