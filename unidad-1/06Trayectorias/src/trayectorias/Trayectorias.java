/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trayectorias;

import java.awt.Color;
import javax.swing.JFrame;
import modelos.Planeta;
import modelos.Puntos;

/**
 *
 * @author OscarZap
 */
public class Trayectorias {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Puntos planetas = new Puntos(); // MODELO
    planetas.add(new Planeta(25, Color.BLUE, 300, 250, 50));
    planetas.add(new Planeta(15, Color.RED, 400, 350, 80));
    planetas.add(new Planeta(10, Color.GREEN, 200, 140, 100));
    planetas.add(new Planeta(50, Color.MAGENTA, 600, 450, 120));
    planetas.add(new Planeta(30, Color.YELLOW, 0, 0, 100));
    //planetas.add(new Planeta(30, Color.ORANGE, 800, 500, 100));
    PanelTrayectorias panel = new PanelTrayectorias(planetas); // VISTA
    OyenteTrayectorias oyente = new OyenteTrayectorias(panel, planetas); // CONTROLADOR
    panel.addEventos(oyente);
    JFrame f = new JFrame("Mini sitema solar");
    f.setSize(800, 600);
    f.setLocation(50, 50);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.add(panel);
    f.setVisible(true);
    }
    
}
